/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdint>
#include <cstdlib>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <iostream>
#include <unistd.h>
#include <thread>
#include <csignal>
#include <chrono>
#include <random>
#include <mutex>

class connection {
public:
    uint16_t rxport;
    uint16_t txport;
    char remote[32];
    size_t packet_size;
    size_t count;
    size_t delay_us;
    uint64_t pkt_sent;
    uint64_t pkt_received;

    connection():
        rxport(0),
        txport(0),
        remote("127.0.0.1"),
        packet_size(1024),
        count(0),
        delay_us(0),
        pkt_sent(0),
        pkt_received(0)
    {
    }
};


bool running = false;
connection conn = connection();
std::mutex mtx;

void
sig_handler(int signal)
{
    if (signal == SIGINT) {
        std::cout << "Sending stop signals..." << std::endl;
        running = false;
    }
}


void
get_args(int argc, char **argv)
{
    bool rxport_set = false;
    bool txport_set = false;
    bool delay_set = false;
    int flag;
    const std::string usage = "Usage: rf-perf [-s|-c host] [options]\n"
                              "  -p  <port> Receiving port \n"
                              "  -r  <host> Remote host to send data. If not set, 127.0.0.1 is used\n"
                              "  -t  <port> Transmitting port \n"
                              "  -i  <size> Packet size. Default 1024   \n"
                              "  -d  <msec> Delay (in milliseconds) before transmitting every frame. 0 to disable";
    while ((flag = getopt(argc, argv, "p:r:i:c:t:d:")) != -1) {
        switch (flag) {
        case 'p':
            conn.rxport = atoi(optarg);
            rxport_set = true;
            break;
        case 't':
            conn.txport = atoi(optarg);
            txport_set = true;
            break;
        case 'r':
            strncpy(conn.remote, optarg, 32);
            break;
        case 'i':
            conn.packet_size = atoi(optarg);
            if (conn.packet_size < sizeof(uint64_t)) {
                throw std::invalid_argument("Packet size should be at least 8 bytes");
            }
            break;
        case 'c':
            conn.count = atoi(optarg);
            break;
        case 'd':
            conn.delay_us = 1000 * atoi(optarg);
            delay_set = true;
            break;
        default:
            throw std::invalid_argument(usage);
            break;
        }
    }
    if (!rxport_set) {
        std::cerr << "Missing RX port" << std::endl;
        throw std::invalid_argument(usage);
    }

    if (!txport_set) {
        std::cerr << "Missing TX port" << std::endl;
        throw std::invalid_argument(usage);
    }
}


void
sender()
{
    struct sockaddr_in remote;
    int tx_sock;
    remote.sin_family = AF_INET;
    remote.sin_port = htons(conn.txport);

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> dis(0, 255);

    if ((tx_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        std::string msg = "socket() failed: " + std::string(std::strerror(errno));
        throw std::runtime_error(msg);
    }

    if (inet_aton(conn.remote, &(remote.sin_addr)) == 0) {
        std::string msg = "inet_aton() failed: " + std::string(std::strerror(errno));
        throw std::runtime_error(msg);
    }

    uint8_t *buffer = new uint8_t[conn.packet_size];
    uint64_t seq = 0;
    while (running) {
        memcpy(buffer, &seq, sizeof(uint64_t));
        seq++;
        for (size_t i = sizeof(uint64_t); i < conn.packet_size; i++) {
            buffer[i] = dis(mt);
        }

        ssize_t ret = sendto(tx_sock, buffer, conn.packet_size, 0,
                             (struct sockaddr *) &remote, sizeof(remote));
        if (ret < 0) {
            std::string msg = "sendto() failed: " + std::string(std::strerror(errno));
            delete [] buffer;
            throw std::runtime_error(msg);
        }
        conn.pkt_sent++;
        std::this_thread::sleep_for(std::chrono::microseconds(conn.delay_us));
    }
    delete [] buffer;
}

void
receiver()
{
    struct sockaddr_in client;
    struct sockaddr_in server;
    int rx_sock;
    server.sin_family = AF_INET;
    server.sin_port = htons(conn.rxport);
    server.sin_addr.s_addr = INADDR_ANY;

    if ((rx_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        std::string msg = "socket() failed: " + std::string(std::strerror(errno));
        throw std::runtime_error(msg);
    }

    if (bind(rx_sock, (const struct sockaddr *) &server, sizeof(server))
            < 0) {
        std::string msg = "bind() failed: " + std::string(std::strerror(errno));
        throw std::runtime_error(msg);
    }

    struct timeval tv;
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    if (setsockopt(rx_sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        std::string msg = "setsockopt() failed: "
                          + std::string(std::strerror(errno));
        throw std::runtime_error(msg);
    }

    std::cout << "Server started receiving at port " << conn.rxport << std::endl;
    uint8_t *buffer = new uint8_t[conn.packet_size];
    while (running) {
        ssize_t ret;
        socklen_t addr_len = sizeof(struct sockaddr_in);
        ret = recvfrom(rx_sock, buffer, conn.packet_size, 0,
                       (struct sockaddr *) &client, &addr_len);
        if (ret < 0) {
            /* Do not panic! Just a timeout occurred */
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                continue;
            }
            std::string msg = "recvfrom() failed: " + std::string(std::strerror(errno));
            delete [] buffer;
            throw std::runtime_error(msg);
        }

        uint64_t seq;
        memcpy(&seq, buffer, sizeof(uint64_t));
        mtx.lock();
        conn.pkt_received++;
        mtx.unlock();
    }
    delete [] buffer;
}

void
print_statistics()
{
    uint64_t last_recv = 0;
    float max_throughput = 0.0;
    float max_frame_loss = 0.0;
    size_t i = 0;
    while (running) {
        mtx.lock();
        float throughput = (conn.pkt_received - last_recv) * conn.packet_size * 8 / 1e6;
        last_recv = conn.pkt_received;
        float frame_loss = 100.0 * (1 - conn.pkt_received / (double)conn.pkt_sent);
        mtx.unlock();
        i++;
        /* Allow warm-up period*/
        if (i > 5) {
            max_throughput = std::max(max_throughput, throughput);
            max_frame_loss = std::max(max_frame_loss, frame_loss);
            std::cout << "===============================================" << std::endl;
            std::cout << "TX Frames      :  " << conn.pkt_sent << std::endl;
            std::cout << "RX Frames      :  " << conn.pkt_received << std::endl;
            std::cout << "Frame loss     :  " << frame_loss << "%" << std::endl;
            std::cout << "Max frame loss :  " << max_frame_loss << "%" << std::endl;
            std::cout << "Throughput     :  " << throughput << " Mbits/s" << std::endl;
            std::cout << "Max throughput :  " << max_throughput << " Mbits/s" << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

int
main(int argc, char **argv)
{
    get_args(argc, argv);

    /* Catch the termination signal*/
    signal(SIGINT, sig_handler);
    running = true;
    std::thread rx(receiver);
    std::thread tx(sender);
    std::thread stats(print_statistics);
    rx.join();
    tx.join();
    stats.join();
}



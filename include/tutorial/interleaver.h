/* -*- c++ -*- */
/*
 * gr-tutorial: Useful blocks for SDR and GNU Radio learning
 *
 *  Copyright (C) 2019, 2020 Manolis Surligas <surligas@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_TUTORIAL_INTERLEAVER_H
#define INCLUDED_TUTORIAL_INTERLEAVER_H

#include <tutorial/api.h>
#include <gnuradio/block.h>

namespace gr {
namespace tutorial {

/*!
 * \brief Block interleaver using the message passing. It received a message
 * containing a payload and perfrorms block interleaving. In case the
 * message size if not a multiple of the block size, it performs zero padding
 *
 * \ingroup tutorial
 *
 */
class TUTORIAL_API interleaver : virtual public gr::block {
public:
    typedef boost::shared_ptr<interleaver> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of tutorial::interleaver.
     *
     * To avoid accidental use of raw pointers, tutorial::interleaver's
     * constructor is in a private implementation
     * class. tutorial::interleaver::make is the public interface for
     * creating new instances.
     */
    static sptr make(size_t block_size = 96);
};

} // namespace tutorial
} // namespace gr

#endif /* INCLUDED_TUTORIAL_INTERLEAVER_H */

